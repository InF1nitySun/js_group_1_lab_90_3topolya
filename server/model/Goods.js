const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GoodsSchema = new mongoose.Schema({
    user : {
        type: Schema.Types.ObjectId, ref:'User'
    },
    title:{
        type: String, required: true
    },
    description:{
        type: String
    },
    status : {
        type: String, required: true
    }
});



mongoose.model('Goods', GoodsSchema);

module.exports = mongoose.model('Goods');